# CLJS-MOLFAR

This is a hobby game written in ClojureScript. The work is in progress. 
You can play the latest version of the game [online](https://manenko.gitlab.io/cljs-molfar/).

The API documentation is available [here](https://manenko.gitlab.io/cljs-molfar/doc/index.html).

## Development

To get an interactive development environment run:

    clj -A:fig:build

This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL.

To clean all compiled files:

    rm -rf target/public

To create a production build run:

    rm -rf target/public
    clj -A:fig:min
    
To generate API documentation in the `target/doc` directory:

    clj -X:codox

### Palette

The game uses [resurrect-64](https://lospec.com/palette-list/resurrect-64) palette for its graphics.

## License

Copyright © 2022 Oleksandr Manenko

Distributed under the Apache License, Version 2.0.
