;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns ^:figwheel-hooks manenko.molfar.main
  (:require
   [cljs.core.async                        :refer [go <!]]
   [goog.dom                               :as    gdom]
   [manenko.molfar.canvas-renderer         :refer [make-renderer
                                                   make-offscreen-renderer]]
   [manenko.molfar.core.asset              :as    asset]
   [manenko.molfar.core.camera             :as    camera]
   [manenko.molfar.core.creature           :as    creature]
   [manenko.molfar.core.game               :as    game]
   [manenko.molfar.core.keyboard           :as    keyboard]
   [manenko.molfar.core.rendering.renderer :as    r]
   [manenko.molfar.core.tilemap            :as    tm]))

(defonce game-state (atom {}))

(def +game-width+        320)
(def +game-height+       200)
(def +game-scale+          3)
(def +game-camera-speed+  32)

(def +scaled-game-width+  (* +game-width+  +game-scale+))
(def +scaled-game-height+ (* +game-height+ +game-scale+))


(defn update-movement! [game-state dt]
  (swap! game-state assoc :scrolled? false)

  ;; Handle movement inputs
  (let [dir (atom {:x 0 :y 0})]
    (when (keyboard/key-pressed? keyboard/+keycode-left+)
      (swap! dir assoc :x -1))
    (when (keyboard/key-pressed? keyboard/+keycode-right+)
      (swap! dir assoc :x  1))
    (when (keyboard/key-pressed? keyboard/+keycode-up+)
      (swap! dir assoc :y -1))
    (when (keyboard/key-pressed? keyboard/+keycode-down+)
      (swap! dir assoc :y  1))

    ;; Update camera if there is any movement requested
    (swap! game-state
           #(-> %
                (creature/move-creature (:hero %)
                                        (:x    @dir)
                                        (:y    @dir)
                                        dt)
                (camera/update-camera [:camera] dt)
                (assoc :scrolled? true)))))

(defn update-state! [game-state dt]
  (update-movement! game-state dt))

(defn render! [game-state]
  (let [tilemap-id (:current-map game-state)
        tilemap    (get-in game-state tilemap-id)
        ss-id      (:spritesheet-id tilemap)
        ss         (get-in game-state ss-id)
        camera     (:camera game-state)
        hero-id    (:hero game-state)
        renderer   (:renderer game-state)]
    (when (:scrolled? game-state)
      (tm/render-tilemap! game-state renderer tilemap-id [:camera]))
    (creature/render-creature! game-state renderer hero-id)
    (r/present! renderer)))

(defn load-assets! [game-state]
  (swap! game-state game/update-game-status :loading)
  (go
    (let [tilemap-id (<! (asset/load-asset! game-state :asset/tilemap  "complex"))
          hero-id    (<! (asset/load-asset! game-state :asset/creature "hero"))]
      (swap! game-state
             #(-> %
                  ;; TODO: Use object layer and spawn player during tilemap creation
                  (creature/spawn-creature hero-id
                                           tilemap-id
                                           32 32)
                  (camera/make-camera [:camera]
                                      hero-id
                                      tilemap-id
                                      +game-width+
                                      +game-height+)
                  (assoc :current-map tilemap-id
                         :hero        hero-id)
                  (game/update-game-status :ready))))))

(defn create-renderers! [game-state]
  (let [game-canvas        (gdom/getElement "canvas")
        game-renderer      (make-renderer game-canvas)
        offscreen-renderer (make-offscreen-renderer +game-width+ +game-height+ game-renderer)]
    (set! (.-width  game-canvas) +scaled-game-width+)
    (set! (.-height game-canvas) +scaled-game-height+)
    ;; Set image scaling quality to :nearest to make game looking old-style.
    (r/set-scale-quality! game-renderer      :nearest)
    (r/set-scale-quality! offscreen-renderer :nearest)
    ;; Save renderers
    (swap! game-state
           assoc
           :game-renderer game-renderer
           :renderer      offscreen-renderer)))

(defonce initialization-block
  (do
    (keyboard/subscribe-to-keyboard-events!)
    (create-renderers! game-state)
    (load-assets! game-state)
    (game/run-game-loop! game-state update-state! render! 60)
    true))
