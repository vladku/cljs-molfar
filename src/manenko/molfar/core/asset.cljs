;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.asset
  "Game asset loading from remote resources.

  **Asset Directory Structure**

  All assets are  stored in the `resources/public/assets`  directory. During the
  deployment phase  this folder  is copied  to the root  of the  web-site, which
  means every asset path starts with `asset/`.

  Each asset type is stored in its directory:

  | Asset Type  | Directory              |
  |-------------|------------------------|
  | Spritesheet | `assets/spritesheets`  |
  | Tilemap     | `assets/maps`          |
  | Creature    | `assets/creatures`     |"
  (:require [cljs.core.async                 :refer [go <!]]
            [cljs.core.async.interop         :refer [<p!]]
            [cljs.reader                     :as reader]
            [manenko.molfar.core.creature    :as cr]
            [manenko.molfar.core.spritesheet :as ss]
            [manenko.molfar.core.tilemap     :as tm]))

(defn load-image!
  "Asynchronously loads a remote image.
  Returns a promise that resolves to the image."
  [src]
  (let [image   (js/Image.)
        promise (js/Promise.
                 (fn [resolve reject]
                   (set! (.-onload  image) #(resolve image))
                   (set! (.-onerror image) #(reject  (str "Cannot load image: " src)))))]
    (set! (.-src image) src)

    promise))

(defn load-json!
  "Asynchronously loads a remote JSON resource and converts it to a Clojure object.
  Returns a promise that resolves to the Clojure object."
  [src]
  (-> (js/fetch src)
      (.then #(.json %))
      (.then #(js->clj % :keywordize-keys true))))

(defn load-edn!
  "Asynchronously loads a remote EDN resource.
  Returns a promise that resolves to the Clojure object."
  [src]
  (-> (js/fetch src)
      (.then #(.text %))
      (.then reader/read-string)))

(defmulti load-asset!
  "Asynchronously loads a remote resource of the given type and name.
  Stores the asset in the given atom map - `game-state`.

  Returns a channel which stores a vector that could be used to access
  the resource in the `game-state` via `get-in`/`update-in` and friends.

  There are a few asset types supported out of the box:

  * `:asset/spritesheet`
  * `:asset/tilemap`
  * `:asset/creature`"
  (fn [game-state type name] type))

(defmethod load-asset! :asset/spritesheet [game-state type name]
  (go
    (let [root         "assets/spritesheets/"
          image-path   (str root name ".png")
          tileset-path (str root name ".tsj")
          image        (<p! (load-image! image-path))
          tileset      (<p! (load-json!  tileset-path))
          id           [:asset/spritesheet name]]
      (swap! game-state ss/make-spritesheet id image tileset)
      id)))

(defn- get-tileset-name [tm]
  ;; We support only a single tileset per map
  (let [source (get-in tm [:tilesets 0 :source])]
    (last (re-find #".*/(.*)\.tsj" source))))

(defmethod load-asset! :asset/tilemap [game-state type name]
  (go
    (let [tilemap-root "assets/maps/"
          tilemap-path (str tilemap-root name ".tmj")
          tilemap      (<p! (load-json! tilemap-path))
          ss-name      (get-tileset-name tilemap)
          ss-id        (<! (load-asset! game-state :asset/spritesheet ss-name))
          id           [:asset/tilemap name]]
      (swap! game-state tm/make-tilemap id tilemap ss-id)
      id)))

(defmethod load-asset! :asset/creature [game-state type name]
  (go
      (let [creature-root "assets/creatures/"
            creature-path (str creature-root name ".edn")
            creature-data (<p! (load-edn! creature-path))
            ss-name       (:spritesheet creature-data)
            ss-id         (<! (load-asset! game-state :asset/spritesheet ss-name))
            id            [:asset/creature name]]
        (swap! game-state
               cr/make-creature
               id
               ss-id
               (:speed      creature-data)
               (:animations creature-data))
        id)))
