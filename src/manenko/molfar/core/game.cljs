; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.game
  "Common game utils.")

(defn game-ready?
  "Checks whether the game's status is `:ready`."
  [game-state]
  (= :ready (:game/status game-state)))

(defn update-game-status
  "Updates the `game-state` with the given status."
  [game-state status]
  (assoc game-state :game/status status))

(defn game-status
  "Returns current game status."
  [game-state]
  (:game/status game-state))

(defn request-animation-frame!
  "Tells the browser that you wish to perform an animation and requests that the
  browser calls a specified function to update an animation before the next
  repaint.

  https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame"
  [fn]
  (.requestAnimationFrame js/window fn))

(defn make-game-loop
  "Returns a function that runs a game loop.

  **Arguments**

  game-state
  : The global game state. Must be an atom.

  update-fn
  : The function of two arguments that updates the `game-state`. It must accept
  two arguments: `game-state` and `dt`, where `game-state` is the `game-state` atom
  and `dt` is delta time passed since the last update and should be used for
  interpolation and things like that.

  render-fn
  : The function of one argument that renders the current `game-state`. It must accept
  a single argument: `game-state` which is the current snapshot of the game state and
  cannot be changed, in other words it's a value of `game-state` atom at this point of
  time.

  max-fps
  : The maximum FPS for the loop. The function will throttle everything above this.

  https://gafferongames.com/post/fix_your_timestep/"
  [game-state update-fn render-fn max-fps]
  (swap! game-state assoc :game-loop/time-acc 0)
  (letfn [(game-loop [elapsed]
            (let [new-time     (/ elapsed 1000) ;; Convert milliseconds to seconds
                  current-time (get @game-state :game-loop/current-time new-time)
                  frame-time   (- new-time current-time)
                  timestep     (/ 1 max-fps)]
              (swap! game-state assoc :game-loop/current-time new-time)
              (swap! game-state update :game-loop/time-acc + frame-time)
              (when (game-ready? @game-state)
                (while (>= (:game-loop/time-acc @game-state) timestep)
                  ;; Perform updates with the fixed timestep as many times as needed
                  (update-fn game-state timestep)
                  (swap! game-state update :game-loop/time-acc #(- % timestep)))
                ;; The game world model is up to date now, it is time to render it
                (render-fn @game-state)))
            (request-animation-frame! game-loop))]
    game-loop))

(defn run-game-loop*!
  "Starts the given game loop function.

  Typically, you do not need to call this function directly, it is more
  convenient to use [[manenko.molfar.game/run-game-loop!]] instead."
  [game-loop-fn]
  (request-animation-frame! game-loop-fn))

(defn run-game-loop!
  "Runs the game loop.

  **Arguments**

  game-state
  : The global game state. Must be an atom.

  update-fn
  : The function of two arguments that updates the `game-state`. It must accept
  two arguments: `game-state` and `dt`, where `game-state` is the original
  `game-state` atom and `dt` is delta time passed since the last update and should
  be used for physics interpolation and things like that.

  render-fn
  : The function of one argument that renders the current `game-state`. It must accept
  a single argument: `game-state` which is the current snapshot of the game state and
  cannot be changed, in other words it's a value of `game-state` atom at this point of
  time.

  max-fps
  : The maximum FPS for the loop. The function will throttle everything above this."
  [game-state update-fn render-fn max-fps]
  (let [game-loop (make-game-loop game-state update-fn render-fn max-fps)]
    (run-game-loop*! game-loop)))

