;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.camera
  "Game camera that can follow a creature and scroll the game world."
  (:require [manenko.molfar.core.tilemap :as tm]))

(defn make-camera
  "Creates a tilemap camera with the given viewport size and configures it
  to follow a creature with `creature-id`.

  **Arguments**

  game-state
  : A global game state.

  camera-id
  : A vector of keys that refers to a place in the `game-state` to store
  the camera entity.

  creature-id
  : A vector of keys that refers to a place in the `game-state` which
  stores a creature the camera must follow, updating its `:screen-x`
  and `:screen-y` coordinates during the update phase.

  tilemap-id
  : A vector of keys that refers to the tilemap in the `game-state` to associate
  the camera with. The camera will display only a part of the map using its
  viewport and provide scrolling capabilities.

  width
  : The width of the camera's viewport.

  height
  : The height of the camera's viewport.

  **Camera Entity Structure**

  | Key            | Description                                                        |
  |----------------|--------------------------------------------------------------------|
  | `:x`           | X-coordinate of the top left corner of visible portion of the map. |
  | `:y`           | Y-coordinate of the top left corner of visible portion of the map. |
  | `:width`       | The width of the camera's viewport.                                |
  | `:height`      | The height of the camera's viewport.                               |
  | `:max-x`       | The maximum horizontal position of the camera.                     |
  | `:max-y`       | The maximum vertical position of the camera.                       |
  | `:creature-id` | The vector of keys that refers to a creature to follow.            |"
  [game-state camera-id creature-id tilemap-id width height]
  (let [tilemap        (get-in game-state tilemap-id)
        tilemap-width  (tm/tilemap-width  tilemap)
        tilemap-height (tm/tilemap-height tilemap)]
    (assoc-in game-state
              camera-id
              {:x           0
               :y           0
               :width       width
               :height      height
               :max-x       (- tilemap-width  width)
               :max-y       (- tilemap-height height)
               :creature-id creature-id})))

(defn- creature-at-left-or-right-side? [creature-x half-width max-x]
  (or (< creature-x half-width)
      (> creature-x (+ max-x half-width))))

(defn- creature-at-top-or-bottom-side? [creature-y half-height max-y]
  (or (< creature-y half-height)
      (> creature-y (+ max-y half-height))))

(defn update-camera
  "Updates the camera and screen coordinates of the creature it follows.

  **Arguments**

  game-state
  : A global game state.

  camera-id
  : A vector of keys that refers to the camera entity in the `game-state`.

  dt
  : Delta time since the last update."
  [game-state camera-id dt]
  (let [camera      (get-in game-state camera-id)
        creature-id (:creature-id camera)
        creature    (get-in game-state creature-id)

        half-width  (/ (:width  camera) 2)
        half-height (/ (:height camera) 2)

        creature-x  (:x creature)
        creature-y  (:y creature)

        max-x       (:max-x camera)
        max-y       (:max-y camera)

        ;; Put the creature at the viewport's center whenever possible
        screen-x    half-width
        screen-y    half-height

        ;; Make the camera follow the creature
        camera-x    (- creature-x half-width)
        camera-y    (- creature-y half-height)

        ;; Clamp camera coordinates
        camera-x    (max 0 (min camera-x max-x))
        camera-y    (max 0 (min camera-y max-y))

        ;; Check if the creature is in the map's sides,  in which case
        ;; it cannot be placed at the center of the screen and we have
        ;; to adjust the coordinates accordingly
        screen-x    (if (creature-at-left-or-right-side? creature-x
                                                         half-width
                                                         max-x)
                      (- creature-x camera-x)
                      screen-x)
        screen-y    (if (creature-at-top-or-bottom-side? creature-y
                                                         half-height
                                                         max-y)
                      (- creature-y camera-y)
                      screen-y)]
    (-> game-state
        ;; Update camera position
        (assoc-in (conj camera-id :x) camera-x)
        (assoc-in (conj camera-id :y) camera-y)
        ;; Update creature screen coordinates
        (assoc-in (conj creature-id :screen-x) screen-x)
        (assoc-in (conj creature-id :screen-y) screen-y))))
