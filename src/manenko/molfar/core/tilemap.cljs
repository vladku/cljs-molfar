;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.tilemap
  "Tilemaps are arrangements of tiles forming meaningful shapes and patterns, such
  as environments or puzzle layouts. Layers allow layering multiple tiles to
  create a broader range of visual effects."
  (:require [manenko.molfar.core.rendering.renderer :as r]
            [manenko.molfar.core.spritesheet        :as ss]))

(defmulti transform-layer
  "Transforms a layer from the given tilemap
  to the representation understood by this game."
  (fn [tilemap layer] (:type layer)))

(defmethod transform-layer "tilelayer" [tilemap layer]
  (let [first-gid (get-in tilemap [:tilesets 0 :firstgid])]
    {:tiles   (mapv
               #(- % first-gid) ;; TODO: Clear all flags too, we don't use them
               (:data layer))
     :id      (:id     layer)
     :name    (:name   layer)
     :columns (:width  layer)
     :rows    (:height layer)
     :type    :tile-layer}))

(defmethod transform-layer "objectgroup" [tilemap layer]
  {:id      (:id      layer)
   :name    (:name    layer)
   :objects (:objects layer)
   :type    :object-layer})

(defmethod transform-layer :default [tilemap layer]
  layer)

(defn- get-tilemap-layers [tiled-tilemap]
  (mapv #(transform-layer tiled-tilemap %) (:layers tiled-tilemap)))

(defn make-tilemap
  [game-state tilemap-id tiled-tilemap spritesheet-id]
  (assoc-in game-state
            tilemap-id
            {:columns        (:width      tiled-tilemap)
             :rows           (:height     tiled-tilemap)
             :tile-width     (:tilewidth  tiled-tilemap)
             :tile-height    (:tileheight tiled-tilemap)
             :spritesheet-id spritesheet-id
             :layers         (get-tilemap-layers tiled-tilemap)}))

(defn empty-tile?
  "Checks whether the given tile is empty and should not be rendered.

  Make sure the tile id was corrected by substracting 'firstgid' from it.
  The manenko.molfar.asset/load-asset! function does this automatically."
  [id]
  (< id 0))

(defn tile-id
  "Returns tile id at the given column and row for the given tilemap layer."
  [layer column row]
  (let [columns (:columns layer)
        index   (+ column (* row columns))]
    (nth (:tiles layer) index -1)))

(defn tile-id-at-xy
  "Returns tile id at the given X and Y coordinates for the given tilemap layer."
  [tilemap layer x y]
  (let [col (.floor js/Math (/ x (:tile-width  tilemap)))
        row (.floor js/Math (/ y (:tile-height tilemap)))]
    (tile-id layer col row)))

(defn obstacle-at-xy?
  "Checks if there is an obstacle at the given coordinates in the game world."
  [tilemap spritesheet x y]
  (->> tilemap
       :layers
       (some #(ss/tile-property spritesheet
                                (tile-id-at-xy tilemap % x y)
                                :obstacle))
       boolean))

(defn tile-column
  "Returns tile columnt at the given X-coordinate."
  [tilemap x]
  (.floor js/Math (/ x (:tile-width tilemap))))

(defn tile-row
  "Returns tile row at the given Y-coordinate."
  [tilemap y]
  (.floor js/Math (/ y (:tile-height tilemap))))

(defn tile-column-x
  "Returns tile column X-coordinate."
  [tilemap column]
  (* column (:tile-width tilemap)))

(defn tile-row-y
  "Returns tile row Y-coordinate."
  [tilemap row]
  (* row (:tile-height tilemap)))

(defn tilemap-width
  "Returns tilemap width in pixels."
  [tilemap]
  (* (:columns    tilemap)
     (:tile-width tilemap)))

(defn tilemap-height
  "Returns tilemap height in pixels."
  [tilemap]
  (* (:rows        tilemap)
     (:tile-height tilemap)))

(defn tilemap-layer-width
  "Returns tilemap layer width in pixels."
  [layer spritesheet]
  (* (:columns    layer)
     (:tile-width spritesheet)))

(defn tilemap-layer-height
  "Returns tilemap layer height in pixels."
  [layer spritesheet]
  (* (:rows        layer)
     (:tile-height spritesheet)))

(defn tilemap-tile-layers
  "Returns tile layers for the given tilemap."
  [tilemap]
  (filter
   #(= (:type %) :tile-layer)
   (:layers tilemap)))

(defn render-tilemap-layer!
  "Renders the given tilemap layer."
  [renderer layer spritesheet camera]
  (let [tile-width   (:tile-width  spritesheet)
        tile-height  (:tile-height spritesheet)

        start-column (.floor js/Math (/ (:x camera) tile-width))
        start-row    (.floor js/Math (/ (:y camera) tile-height))

        end-column   (+ start-column (/ (:width  camera) tile-width))
        end-row      (+ start-row    (/ (:height camera) tile-height))

        offset-x     (- (* start-column tile-width)  (:x camera))
        offset-y     (- (* start-row    tile-height) (:y camera))]
    (doseq [r (range start-row    (+ 1 end-row))
            c (range start-column (+ 1 end-column))]
      (let [tile-id (tile-id layer c r)]
        (when-not (empty-tile? tile-id)
          (let [x (+ offset-x (* tile-width  (- c start-column)))
                y (+ offset-y (* tile-height (- r start-row)))]
            (ss/render-tile! renderer
                             spritesheet
                             tile-id
                             (.round js/Math x)
                             (.round js/Math y))))))))

(defn render-tilemap!
  "Renders all tile layers of the given tilemap."
  [game-state renderer tilemap-id camera-id]
  (let [tilemap     (get-in game-state tilemap-id)
        spritesheet (get-in game-state (:spritesheet-id tilemap))
        camera      (get-in game-state camera-id)
        tile-layers (tilemap-tile-layers tilemap)] ;; TODO: filter out invisible layers?
    (r/clear! renderer)
    (doseq [layer tile-layers]
      (render-tilemap-layer! renderer layer spritesheet camera))))
