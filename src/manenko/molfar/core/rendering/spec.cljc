(ns manenko.molfar.core.rendering.spec
  "Specs for [[manenko.molfar.core.rendering]]."
 (:require [clojure.spec.alpha :as spec]))

(spec/def :renderer/size
  (spec/coll-of (spec/int-in 0 268435456)
                :kind  vector?
                :count 2))

(spec/def :renderer/color
  (spec/coll-of (spec/int-in 0 256)
                :kind  vector?
                :count 4))

(spec/def :renderer/scale-quality #{:nearest
                                    :linear
                                    :best})
