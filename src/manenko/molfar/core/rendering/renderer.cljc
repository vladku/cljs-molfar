(ns manenko.molfar.core.rendering.renderer
  "Generic protocols, data types, and functions related to graphics renderers.")

(defprotocol ARenderer
  "A rendering context used to send rendering commands to a rendering target.

  A rendering target is something we draw things on. It could a screen, texture,
  or anything else your renderer supports."
  (output-size [this]
    "Returns a vector of two numbers: `width` and `height` of the rendering
 context in pixels.

Do not cache this value, as it may change throughout the lifecycle of the
renderer.")

  (scale-quality [this]
    "Returns image/texture scaling quality and could be either of `:nearest`,
    `:linear`, or `:best`.

The actual value depends on the renderer.")

  (set-scale-quality! [this scale-quality]
    "Changes the image/texture scaling property. The `scale-quality` could be
    either of `:nearest`, `:linear`, or `:best`.

The renderer may not support some of these values in which case the method
returns logical false and does not change the quality.")

  (clear! [this]
    "Clears the current rendering target erasing its pixels.")

  (render-image! [this image src-x src-y src-width src-height dst-x dst-y dst-width dst-height]
    "Renders the given image to the current rendering target.

#### Arguments

image
: The image to render to the current rendering target. The type of this object
is transparent to the protocol. Each implementation of the protocol will use
their own specific type to represent an 'image'.

src-x
: The X coordinate of the top left corner of the sub-rectangle of the source
image to render.

src-y
: The Y coordinate of the top left corner of the sub-rectangle of the source
image to render.

src-width
: The width of the sub-rectangle of the source image to render.

src-height
: The height of the sub-rectangle of the source image to render.

dst-x
: The X coordinate in the rendering target at which to place the top-left corner
of the source image.

dst-y
: The Y coordinate in the rendering target at which to place the top-left corner
of the source image.

dst-width
: The width to render the image in the rendering target. This allows scaling of
the rendered image.

dst-height
: The height to render the image in the rendering target. This allows scaling of
the rendered image.")

  (present! [this]
    "Updates the rendering target with any rendering performed since the previous call."))
