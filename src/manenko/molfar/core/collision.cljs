;; Copyright 2022 Oleksandr Manenko
;;
;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(ns manenko.molfar.core.collision
  "Collision detection in two-dimensional space.")

(defn point-in-rect?
  "Checks if the given point is inside the rectangle.

  **Arguments**

  x
  : The X-coordinate of the point.

  y
  : The Y-coordinate of the point.

  rect-x
  : The X-coordinate of the top-left corner of the rectangle.

  rect-y
  : The Y-coordinate of the top-left corner of the rectangle.

  rect-width
  : The width of the rectangle.

  rect-height
  : The height of the rectangle."
  [x y rect-x rect-y rect-width rect-height]
  (and (>= x rect-x)
       (<= x (+ rect-x rect-width))
       (>= y rect-y)
       (<= y (+ rect-y rect-height))))

(defn rects-intersect?
  "Checks if two rectangles intersect.

  **Arguments**

  r1-x
  : The X-coordinate of the top-left corner of the first rectangle.

  r1-y
  : The Y-coordinate of the top-left corner of the first rectangle.

  r1-width
  : The width of the first rectangle.

  r1-height
  : The height of the first rectangle.

  r2-x
  : The X-coordinate of the top-left corner of the second rectangle.

  r2-y
  : The Y-coordinate of the top-left corner of the second rectangle.

  r2-width
  : The width of the second rectangle.

  r2-height
  : The height of the second rectangle.

  **Algorithm**

  To detect collision between two rectangles we can check if the two rectangles
  X values (left + right) overlap. We can also check if the rectangles Y
  values (top + bottom) overlap. If an overlap occurs between both X and Y
  values the two rectangles are overlapping, otherwise they are not.

  The following diagram demonstrates four cases:

  1. There is no overlapping of either of X or Y.
  2. There is overlapping of X but not Y.
  3. There is overlapping of Y but not X.
  3. Both X and Y overlap making rectangles overlap.

  ```
  ┌──────────────────────┐ ┌──────────────────────┐
  │┌──────────┐        1 │ │┌──────────┐        2 │
  ││          │          │ ││          │          │
  ││          │          │ ││          │          │
  ││          │          │ ││          │          │
  │└──────────┘          │ │└──────────┘          │
  │             ┌────┐   │ │        ┌────┐        │
  │             │    │   │ │        │    │        │
  │             └────┘   │ │        └────┘        │
  └──────────────────────┘ └──────────────────────┘
  ┌──────────────────────┐ ┌──────────────────────┐
  │┌──────────┐        3 │ │┌──────────┐        4 │
  ││          │          │ ││          │          │
  ││          │          │ ││          │          │
  ││          │ ┌────┐   │ ││       ┌──┴─┐        │
  │└──────────┘ │    │   │ │└───────┤    │        │
  │             └────┘   │ │        └────┘        │
  │                      │ │                      │
  │                      │ │                      │
  └──────────────────────┘ └──────────────────────┘
  ```

  Given two rectangles, A and B. The X values overlap if A's left value is
  less than B's right and A's right value is greater than B's left.

  Similarly the X values overlap if A's top value is less than B's bottom and
  A's bottom is greater than B's top."
  [r1-x r1-y r1-width r1-height
   r2-x r2-y r2-width r2-height]
  (let [r1-right  (+ r1-x r1-width)
        r1-bottom (+ r1-y r1-height)
        r2-right  (+ r2-x r2-width)
        r2-bottom (+ r2-y r2-height)]
  (and (<= r1-x r2-right)
       (>= r1-right r1-x)
       (<= r1-y r2-bottom)
       (>= r1-bottom r2-y))))
